#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/opt/google-cloud-sdk/bin/:/Users/yentingche/google-cloud-sdk/bin
export PATH

gcp_project=$(cat vars/elasticsearch.yml| grep gcp_project | awk '{print $2}')

case $gcp_project in
    projects-1-id) service_account=projects-1_SERVICE_ACCOUNT
    gcloud auth activate-service-account --key-file $projects_1_SERVICE_ACCOUNT
        ;;
    projects-2-id) service_account=projects-2_SERVICE_ACCOUNT
    gcloud auth activate-service-account --key-file $projects_2_SERVICE_ACCOUNT
        ;;
    projects-3-id) service_account=projects-3_SERVICE_ACCOUNT
    gcloud auth activate-service-account --key-file $projects_3_SERVICE_ACCOUNT
        ;;
    *) echo "unknow poject $gcp_project"
esac